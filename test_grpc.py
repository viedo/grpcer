from src import *


def cb1(bytes):
    return (bytes.decode() + ' ok.').encode()


def cb2(msg):
    for k, v in msg.items():
        print(f'{k}: {v}')
    msg['v'] *= 1.5
    return msg


if __name__ == '__main__':
    s = Grpcer()
    s.start_server(cb2)

    c = Grpcer()
    m = {
        'a': 'abc',
        'v': 44.54,
    }

    r = c.send(s, m)
    print(type(r), r)
